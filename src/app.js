const {getTodosApi,getTodosUsers} = require('../src/api.js')
const createTodos = require('../src/func.js')

let usersListArr

getTodosUsers().then((result) => {
    let data = result.json()
    return data
}).then((data) => {
    usersListArr = data
}).then((data) => {
    return getTodosApi()
}).then((todos) => {
    let todoData = todos.json()
    return todoData
}).then((todosList) => {
    return createTodos(todosList,usersListArr)
}).catch((err) => {
    console.log(err)
})

