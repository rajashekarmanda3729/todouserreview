let todoContainerEl = document.getElementById('todoContainer')

function createTodos(todosList,usersListArr) {

    todosListArr = todosList
    let todosUsersGroupList = []

    usersListArr.map(eachUser => {
        let todosTasksArr = []

        todosListArr.map(eachTodo => {

            if (eachUser.id === eachTodo.userId) {
                todosTasksArr.push(eachTodo.title)
            }
        })
        todosUsersGroupList.push({ [eachUser.username]: todosTasksArr })
    })

    let count = 0
    let number = 1
    todosUsersGroupList.map(eachUserTodo => {

        Object.entries(eachUserTodo).map(eachTodo => {

            let userEl = document.createElement('h3')
            userEl.textContent = `${number} ${eachTodo[0]}`
            todoContainerEl.appendChild(userEl)
            let todosLimit = 0

            eachTodo[1].map(task => {

                if (todosLimit < 5) {
                    let divEl = document.createElement('div')
                    userEl.appendChild(divEl)

                    let checkbox = document.createElement('input')
                    checkbox.setAttribute('type', "checkbox")
                    checkbox.setAttribute('id', `checkbox${count + 1}`)
                    divEl.appendChild(checkbox)

                    let label = document.createElement('label')
                    label.setAttribute('for', `checkbox${count + 1}`)
                    label.textContent = task
                    divEl.appendChild(label)
                    todosLimit++
                }
            })
            count++
        })
        number++
    })
}

module.exports = createTodos