function getTodosApi() {
    let todoTasks = fetch('https://jsonplaceholder.typicode.com/todos')
    return todoTasks
}

function getTodosUsers() {
    let todoUsers = fetch('https://jsonplaceholder.typicode.com/users')
    return todoUsers
}

module.exports = {getTodosApi,getTodosUsers}